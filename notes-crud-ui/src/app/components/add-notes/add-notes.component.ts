import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


import { NoteService } from 'src/app/services/note.service';

@Component({
  selector: 'app-add-notes',
  templateUrl: './add-notes.component.html',
  styleUrls: ['./add-notes.component.css']
})
export class AddNotesComponent implements OnInit {

  note = {
    title: '',
    description: '',
    published: false
  };
  submitted = false;

  constructor(private noteService: NoteService,
              private router: Router,
              private activatedRoute: ActivatedRoute
    ) { }

  ngOnInit(): void {
  }

  saveNotes() {
    const data = {
      title: this.note.title,
      description: this.note.description
    };

    this.noteService.create(data)
    .subscribe(
      res => {
        this.submitted = true;
        this.router.navigateByUrl('/notes');


      },
      err => {
        console.log(err);
      });
    }

    newNote() {
      this.submitted = false;
      this.note = {
        title: '',
        description: '',
        published: false
      };
    }

}
