import { Component, OnInit } from '@angular/core';
import { NoteService } from 'src/app/services/note.service';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.css']
})
export class NoteListComponent implements OnInit {

  notes: any;
  currentNote = null;
  currentIndex = -1;
  title = '';

  constructor(private noteService: NoteService) { }

  ngOnInit(): void {
    this.retrieveNotes();
  }

  retrieveNotes() {
    this.noteService.getAll()
    .subscribe(
      data => {
        this.notes = data;
        console.log(data);
      },
      err => {
        console.log(err);
      }
    );
  }

  refreshList() {
    this.retrieveNotes();
    this.currentNote = null;
    this.currentIndex = -1;
  }

  setActiveNote(note, index) {
    this.currentNote = note;
    this.currentIndex = index;
  }

  removeAllNotes() {
    this.noteService.deleteAll()
    .subscribe(
      res => {
        console.log(res);
        this.retrieveNotes();
      },
      err => {
        console.log(err);
      }
    );
  }

  searchTitle() {
    this.noteService.findByTitle(this.title)
    .subscribe(
      data => {
        this.notes = data;
        console.log(data);
      },
      err => {
        console.log(err);
      });
  }


}
