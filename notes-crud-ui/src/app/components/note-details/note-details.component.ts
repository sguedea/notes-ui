import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NoteService } from 'src/app/services/note.service';

@Component({
  selector: 'app-note-details',
  templateUrl: './note-details.component.html',
  styleUrls: ['./note-details.component.css']
})
export class NoteDetailsComponent implements OnInit {

  currentNote = null;
  message = '';

  constructor(private noteService: NoteService,
              private route: ActivatedRoute,
              private router: Router
              ) { }

  ngOnInit(): void {
    this.message = '';
    this.getNote(this.route.snapshot.paramMap.get('id'));
  }

  getNote(id) {
    this.noteService.get(id)
    .subscribe(
      data => {
        this.currentNote = data;
        console.log(data);
      },
      err => {
        console.log(err);
      });
  }

  updatePublished(status) {
    const data = {
      title: this.currentNote.title,
      description: this.currentNote.description,
      published: status
    };

    this.noteService.update(this.currentNote.id, data)
    .subscribe(
      res => {
        this.currentNote.published = status;
        console.log(res);
      },
      err => {
        console.log(err);
      });
  }

  updateNote() {
    this.noteService.update(this.currentNote.id, this.currentNote)
    .subscribe(
      res => {
        console.log(res);
        this.message = 'The note was updated successfully';
      },
      err => {
        console.log(err);
      });
  }

  deleteNote() {
    this.noteService.delete(this.currentNote.id)
    .subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/notes']);
      },
      err => {
        console.log(err);
      });
  }

}
