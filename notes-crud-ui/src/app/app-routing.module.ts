import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NoteListComponent } from './components/note-list/note-list.component';
import { NoteDetailsComponent } from './components/note-details/note-details.component';
import { AddNotesComponent } from './components/add-notes/add-notes.component';

const routes: Routes = [
  { path: '', redirectTo: 'note', pathMatch: 'full'},
  { path: 'notes', component: NoteListComponent},
  { path: 'notes/:id', component: NoteDetailsComponent},
  { path: 'add', component: AddNotesComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
